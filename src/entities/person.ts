export class Person {
                      // on déclare les pros à l'extérieur du constructor et on les type. 
  // public name:string;
  // public surname:string;
  // private age: number;

  // constructor(name:string, surname:string, age:number){

  //   this.name = name;
  //   this.surname = surname;
  //   this.age = age;

  // }

  // bonAnniversaire(){   ici on peut utiliser la propriété privée age.
  //   this.age ++;
  // }

  // on met tout directement ds le constructor, y compris le constructor de façon à ne pas avoir à répéter cf au dessus.
constructor(public name:string,  
            public surname:string,
            private age: number = 0){

}

presentation():string { // on précise que le return doit renvoyer du strict // on type le return
  return `coucou, je m'appelle ${this.name}, j'ai ${this.age} ans`;
}
}
// let instance = new Person();  
// instance.name = "Demel";
