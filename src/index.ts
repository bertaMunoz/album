import * as $ from 'jquery';
import { Person } from './entities/person';
// importe moi tout ce qu'il y a ds jquery et tu le mets sans un variable $
console.log("bloup");

let maVariable:any = "doudidou";
maVariable = 123;

let surname:string  = "blouuup"  // c'est mieux de typer explicitement la variable même si ce n'est pas nécessaire. 
console.log(surname);

//let surname:string;
//surname = "bloup";

let input:HTMLInputElement; // On peut utiliser d'autres choses que les type basics. 

let formateur:Person = new Person("Demel", "Jean", 20); // ne pas oublier d'importer Person (cf en haut). // ici on dit que formateur est fde type person // age, même si je n'y ait pas acces en lecture c'est quand meme un argument du constructor. 
console.log(formateur.presentation());
